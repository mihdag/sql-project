-- David Hang, 2234338, Dang Minh Nguyen, 2133544
-- Drop all the tables created by Setup.sql
DROP TABLE address;
DROP TABLE email;
DROP TABLE orders;
DROP TABLE reviews;
DROP TABLE inventory;
DROP TABLE audit_log;
DROP SEQUENCE seq_audit_id;
DROP TABLE products;
DROP TABLE warehouse;
DROP TABLE customers;

-- Drop other types and procedures.
DROP PACKAGE customer_data;
DROP PACKAGE inventory_data;
DROP PACKAGE order_data;
DROP PACKAGE product_data;
DROP PACKAGE review_data;
DROP PACKAGE warehouse_data;
DROP PACKAGE display_data;
DROP PACKAGE function_data;


