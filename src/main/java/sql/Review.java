// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Review {
    private int review_id; 
    private int review_flag;
    private String description;
    private int review;  

    public Review(){

    }

    public Review(int review_id, int review_flag, String description, int review) {
        this.review_id = review_id;
        this.review_flag = review_flag;
        this.description = description;
        this.review = review;
    }

    @Override
    public String toString() {
        return
            " review_id = '" + getReview_id() + "'" +
            ", review_flag = '" + getReview_flag() + "'" +
            ", description = '" + getDescription() + "'" +
            ", review='" + getReview() + "'";
    }

    public int getReview_id() {
        return this.review_id;
    }

    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }

    public int getReview_flag() {
        return this.review_flag;
    }

    public void setReview_flag(int review_flag) {
        this.review_flag = review_flag;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReview() {
        return this.review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    //Function to get average rating of a product
    public static double getAverageRating (Connection conn) throws SQLException {
        System.out.println("What product you are looking for?");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        sc.close();
        String query = "SELECT * FROM reviews JOIN orders ON reviews.review_id = orders.order_id JOIN products ON orders.product_id = products.product_id WHERE name = '" + name + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int sum = 0;
        int count = 0;
        while (rs.next()) {
            sum = sum + rs.getInt("rating");
            count++;
        }
        return sum/count;
    }

    //get all customers that got flagged more than once
    public static ArrayList<Customer> getFlaggedCustomers (Connection conn) throws SQLException {
        String query = "SELECT DISTINCT * FROM customers JOIN orders ON customers.customer_id = orders.customer_id JOIN reviews ON orders.review_id = reviews.review_id WHERE review_flag > 0";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<Customer> customers = new ArrayList<Customer>();
        while (rs.next()) {
            String firstName = rs.getString("firstname");
            String lastName = rs.getString("lastname");
            Customer customer = new Customer(firstName, lastName);
            customers.add(customer);
        }
        return customers;
    }

    //Display all review made for a product
    public static ArrayList<Review> displayReviewByProduct (Connection conn) throws SQLException {
        System.out.println("What ProductID would you like to display their reviews ?");
        Scanner sc = new Scanner(System.in);
        int ID = sc.nextInt();
        String query = "SELECT * FROM products p JOIN orders o ON p.product_id = o.product_id JOIN reviews r ON r.review_id = o.review_id WHERE o.product_id = " + ID;
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<Review> reviews = new ArrayList<Review>();
        while (rs.next()) {
            int review_id = rs.getInt("review_id");
            int review_flag = rs.getInt("review_flag");
            int review = rs.getInt("rating");
            String description = rs.getString("description");
            Review rev = new Review(review_id,review_flag,description,review);
            reviews.add(rev);
        }
        return reviews;

    }

    // add data for review
    public static void addReview(Connection conn) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("What's the review flag?");
        int review_flag = sc.nextInt();
        System.out.println("What's your description?");
        sc.nextLine();
        String review = sc.nextLine();
        System.out.println("What's your rating?");
        int rating = sc.nextInt();
        sc.close();

        String query = "SELECT COUNT(*) FROM reviews";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("COUNT(*)");
        }
        count = count + 1;

        // Use the procedure add_review_product from package review_data to add a review
        String stm = String.format("{call review_data.add_review(%d,%d,'%s', %d)}",count, review_flag, review, rating);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("review added successfully.");

           }
    }

    // used to update review
    public static void updateReview(Connection conn) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("What is the reviewID that you want to update?");
        int ID = sc.nextInt();
        System.out.println("What's updated review flag?");
        int review_flag = sc.nextInt();
        System.out.println("What's your updated description?");
        sc.nextLine();
        String review = sc.nextLine();
        System.out.println("What's the updated rating?");
        int rating = sc.nextInt();
        sc.close();

        // Use the procedure add_review_product from package review_data to add a review
        String stm = String.format("{call review_data.update_review(%d,%d,'%s', %d)}",ID, review_flag, review, rating);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("review updated successfully.");

           }
    }

    // used to delete review
    public static void deleteReview(Connection conn) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("What is the reviewID that you want to delete?");
        int ID = sc.nextInt();

        // Use the procedure add_review_product from package review_data to add a review
        String stm = String.format("{call review_data.delete_review(%d)}",ID);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("review deleted successfully.");

           }
    }
}
