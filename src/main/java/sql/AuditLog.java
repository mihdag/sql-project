// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;


public class AuditLog {
    private int audit_id;
    private String table_name;
    private String operation;
    private String user_name;
    private Timestamp operationTime;
    private String details;

    public AuditLog() {
    }

    //Constructor
    public AuditLog(int audit_id, String table_name, String operation, String user_name, Timestamp operationTime, String details) {
        this.audit_id = audit_id;
        this.table_name = table_name;
        this.operation = operation;
        this.user_name = user_name;
        this.operationTime = operationTime;
        this.details = details;
    }

    @Override
    public String toString() {
        return
            " audit_id : " + getAudit_id() +
            ", table_name : " + getTable_name() +
            ", operation : " + getOperation() +
            ", user_name : " + getUser_name() +
            ", operationTime : " + getOperationTimel() +
            ", details : " + getDetails();
    }


// Getters and setters
    public int getAudit_id() {
        return this.audit_id;
    }

    public void setAudit_id(int audit_id) {
        this.audit_id = audit_id;
    }

    public String getTable_name() {
        return this.table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getOperation() {
        return this.operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getUser_name() {
        return this.user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Timestamp getOperationTimel() {
        return this.operationTime;
    }

    public void setOperationTimel(Timestamp operationTime) {
        this.operationTime = operationTime;
    }

    public String getDetails() {
        return this.details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    //Methods used to display all the logs
        public static ArrayList<AuditLog> displayAuditLog (Connection conn) throws SQLException {
        ArrayList<AuditLog> auditLogs = new ArrayList<AuditLog>();
        String query = "SELECT * FROM audit_log";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {

            int audit_id = rs.getInt("audit_id");
            String table_name = rs.getString("table_name");
            String operation = rs.getString("operation");
            String user_name = rs.getString("user_name");
            Timestamp operationTime = rs.getTimestamp("operation_time");
            String details = rs.getString("details");

            AuditLog auditLog = new AuditLog(audit_id, table_name, operation, user_name, operationTime, details);
            auditLogs.add(auditLog);


        }
        return auditLogs;
}
}
