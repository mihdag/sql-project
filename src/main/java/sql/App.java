package sql;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException
    {
        Scanner sc = new Scanner(System.in);
        System.out.println( "Hello World!" );
        System.out.println("What's your username?");
        String username = sc.nextLine();
        System.out.println("What's your password?");
        String password = sc.nextLine();
        ConnectionDB db = new ConnectionDB(username, password);
        Connection connection = db.getConnection();

        //Interface for user to interact with the database
        String validateOption = "12345";
        System.out.println("Select the following option");
        System.out.println("1. Display data\n" + //
        "2. Functions\n" + //
        "3. Add data\n" + //
        "4. Update data\n" + //
        "5. Delete Data");



        // If not in the option let user try again
        String option = getOptionString(validateOption);

        switch (option) {

            // Case for Display 
            case "1":
                    System.out.println("1. Display quantity of a product\n" + //
                        "2. Display orders from local store \n" + //
                        "3. Display orders from certain customer\n" + //
                        "4. Display all products\n" + //
                        "5. display products based on category\n" +
                        "6. Display all review description for a product \n" +
                        "7. Display logs");
                        validateOption = "1234567";
                        String option1 =getOptionString(validateOption);

                switch (option1) {
                    case "1":

                        int quant = Inventory.checkInventory(connection);
                        System.out.println("The quantity remains is: " + quant);

                        break;
                    case "2":
                        ArrayList<Order> orders = Order.displayOrderByStore(connection);
                        for(Order o : orders){
                           System.out.println(o.toString());
                        }
                        break;
                    case "3":
                        System.out.println("Display orders from certain customer");
                        ArrayList<Order> orders1 = Order.displayOrderByCustomer(connection);
                        for(Order o : orders1){
                           System.out.println(o.toString());
                        }
                        break;
                    case "4":
                        ArrayList<Product> prod =  Product.displayAllProducts(connection);
                        for(Product p : prod){
                           System.out.println(p.toString());
                        }
                        break;
                    case "5":
                         ArrayList<Product> pr = Product.displayProductBYcategory(connection);
                            for(Product p: pr){
                                String name = p.getName();
                                System.out.println(name);
                            }
                        break;
                    case "6":
                        ArrayList<Review> reviews = Review.displayReviewByProduct(connection);
                            for(Review r: reviews){
                                System.out.println(r.toString());
                            }
                        break;
                    case "7":
                        ArrayList<AuditLog> auditLogs = AuditLog.displayAuditLog(connection);

                        for(AuditLog log : auditLogs){
                           System.out.println(log.toString());
                        }
                        break;

                    
                    default:
                        break;
                }
            break;

            //Case for functions
            case "2":
                System.out.println("1. get flagged customers\n" + //
                        "2. average rating \n" + //
                        "3. validate order\n" + //
                        "4. get number of orders by product");
                        validateOption = "1234";
                        String option2 =getOptionString(validateOption);

                        switch (option2) {
                            case "1":
                                ArrayList<Customer> customers= Review.getFlaggedCustomers(connection);
                                for(Customer c: customers){
                                    String firstName = c.getFirstName();
                                    String lastName = c.getLastName();
                                    System.out.println(firstName + " " + lastName + "\n");
                                }
                                break;
                            case "2":
                                System.out.println("The average rating is: " + Review.getAverageRating(connection));
                                break;
                            case "3":
                                Order.validateOrder(connection);
                                break;
                            case "4":
                                System.out.println("The number of orders is: " + Order.getNumberOfOrders(connection));
                                break;
                            default:
                                break;
                        }
            break;
        
            //Case for adding data
            case "3":
                      System.out.println("1. Add order\n" + //
                        "2. Add a product\n" + //
                        "3. Add review\n" + //
                        "4. Add inventory");
                         validateOption = "1234";
                        String option3 =getOptionString(validateOption);

                        switch (option3) {
                            case "1":
                                Order.addOrder(connection);
                                break;
                            case "2":
                                Product.addProduct(connection);
                                break;
                            case "3":
                                Review.addReview(connection);
                                break;
                            case "4":
                                Inventory.addInventory(connection);
                                break;
                            default:
                                break;
                        }
            break;

            //Case for updating data
            case "4":
                      System.out.println("1. Update warehouse\n" + //
                        "2. Update review\n" + //
                        "3. Update product\n" + //
                        "4. Update inventory quantity ");
                         validateOption = "1234";
                        String option4 =getOptionString(validateOption);

                        switch (option4) {
                            case "1":
                                Warehouse.updateWarehouse(connection);
                                break;
                            case "2":
                                Review.updateReview(connection);
                                break;
                            case "3":
                                Product.updateProduct(connection);
                                break;
                            case "4":
                                Inventory.updateInventory(connection);
                                break;
                        
                            default:
                                break;
                        }
            break;

            //Case for deleting data
            case "5":
                      System.out.println("1. delete product \n" + //
                        "2. delete order\n" + //
                        "3. delete review\n" + //
                        "4. delete warehouse ");
                         validateOption = "1234";
                        String option5 =getOptionString(validateOption);

                        switch (option5) {
                            case "1":
                                Product.deleteProduct(connection);
                                break;
                            case "2":
                                Order.deleteOrder(connection);
                                break;
                            case "3":
                                Review.deleteReview(connection);
                                break;
                            case "4":
                                Warehouse.deleteWarehouse(connection);
                                break;
                        
                            default:
                                break;
                        }
            break;

            default:
                break;
            }
    sc.close();
}
        


    // used for switch cases
    public static String getOptionString(String validateOption) {
        Scanner sc = new Scanner(System.in);
        String option;
            do{
                option = sc.next();
                if(!validateOption.contains(option)){
                    System.out.println("Invalid option! Try Again" );
                }  
            }while(!validateOption.contains(option));
    return option;
    }
}
