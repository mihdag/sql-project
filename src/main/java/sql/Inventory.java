// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import oracle.jdbc.*;

import oracle.jdbc.oracore.OracleType;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Inventory {
    private String warehouse_id ;
    private int product_id;
    private int quantity;


    public Inventory(String warehouse_id, int product_id, int quantity) {
        this.warehouse_id = warehouse_id;
        this.product_id = product_id;
        this.quantity = quantity;
    }


    @Override
    public String toString() {
        return "{" +
            " warehouse_id='" + getWarehouse_id() + "'" +
            ", product_id='" + getProduct_id() + "'" +
            ", quantity='" + getQuantity() + "'" +
            "}";
    }

    public String getWarehouse_id() {
        return this.warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public int getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    //to retrieve data from inventory
public static int checkInventory (Connection conn) throws SQLException {
        System.out.println("What product you are looking for?");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        sc.close();
        String query = "SELECT * FROM products JOIN inventory ON products.product_id = inventory.product_id WHERE name = '" + name + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int quantity = 0;
        while (rs.next()) {
            quantity = rs.getInt("quantity");
        }
        System.out.println("There are " + quantity + " left in stock");
        return quantity;
    }

    //update from inventory
    public static void updateInventory (Connection conn) throws SQLException {
        System.out.println("What warehouse you want to update?");
        Scanner sc = new Scanner(System.in);
        String warehouse_id = sc.nextLine();
        System.out.println("What productID you want to update?");
        int product_id = sc.nextInt();
        System.out.println("What quantity you want to update?");
        int quantity = sc.nextInt();
        sc.close();
        
    //update_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT);
    String stm = String.format("{call inventory_data.update_inventory('%s', %d, %d)}", warehouse_id, product_id, quantity);
    try (CallableStatement callableStatement = conn.prepareCall(stm)) {
        // Execute the stored procedure
        callableStatement.executeUpdate();

        System.out.println("Product updated successfully.");

        }
    }

    //add data to inventory
    public static void addInventory (Connection conn) throws SQLException {
        System.out.println("What warehouse would you like to add the inventory?");
        Scanner sc = new Scanner(System.in);
        String warehouse_id = sc.nextLine();
        System.out.println("What productID you want to add?");
        int product_id = sc.nextInt();
        System.out.println("What quantity would you like to add?");
        int quantity = sc.nextInt();
        sc.close();
        
    //update_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT);
    String stm = String.format("{call inventory_data.add_inventory('%s', %d, %d)}", warehouse_id, product_id, quantity);
    try (CallableStatement callableStatement = conn.prepareCall(stm)) {
        // Execute the stored procedure
        callableStatement.executeUpdate();

        System.out.println("Product Added successfully.");

        }
    }
}