// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.CallableStatement;

public class Product {
    private int product_id;
    private String name;
    private String category;


    public Product(int product_id, String name, String category) {
        this.product_id = product_id;
        this.name = name;
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product ID: " + this.product_id + "\nItem: " + this.name + "\nCategory: " + this.category + "\n";
    }


    public int getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    //Display all product in one category
    public static ArrayList<Product> displayProductBYcategory (Connection conn) throws SQLException {
        System.out.println("What category you are looking for?");
        Scanner sc = new Scanner(System.in);
        String cat = sc.nextLine();
        ArrayList<Product> products = new ArrayList<Product>();
        sc.close();
        String query = "SELECT * FROM products WHERE category = '" + cat + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            int product_id = rs.getInt("product_id");
            String name = rs.getString("name");
            String category = rs.getString("category");
            Product product = new Product(product_id, name, category);
            products.add(product);
        }
        return products;
}

// Add products
    public static void addProduct (Connection conn) throws SQLException {
        System.out.println("What product you are adding?");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        System.out.println("What category you are adding?");
        String category = sc.nextLine();
        sc.close();
        String query = "SELECT COUNT(*) FROM products";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("COUNT(*)");
        }
        count = count + 1;

        String stm = String.format("{call product_data.add_product(%d, '%s', '%s')}", count, name, category);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("Product added successfully.");

           }
    }

    //update products
    public static void updateProduct (Connection conn) throws SQLException {
        System.out.println("What's the ID of the product you are changing?");
        Scanner sc = new Scanner(System.in);
        int ID = sc.nextInt();
        sc.nextLine();
        System.out.println("What do you want his name to be?");
        String name = sc.nextLine();
         System.out.println("What do you want his category to be?");
        String category = sc.nextLine();
        sc.close();

        String stm = String.format("{call product_data.update_product(%d, '%s', '%s')}", ID, name, category);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("Product updated successfully.");

           }
    }

    //delete products
    public static void deleteProduct (Connection conn) throws SQLException {
        System.out.println("What productID would you like to delete?");
        Scanner sc = new Scanner(System.in);
        int ID = sc.nextInt();
        sc.close();
        String stm = String.format("{call product_data.delete_product(%d)}",ID);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("Product deleted successfully.");

           }
    }

      public static ArrayList<Product> displayAllProducts(Connection conn) throws SQLException {
        ArrayList<Product> products = new ArrayList<Product>();
        String query = "SELECT * FROM products";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            int product_id = rs.getInt("product_id");
            String name = rs.getString("name");
            String category = rs.getString("category");
            Product product = new Product(product_id, name, category);
            products.add(product);
        }
        return products;
    }

}
