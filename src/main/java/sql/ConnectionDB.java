// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
    private String username;
    private String password;

    //used to connect the java to the database
    public ConnectionDB(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;

        String URL = "jdbc:oracle:thin:@198.168.52.211: 1521/pdbora19c.dawsoncollege.qc.ca";
        conn = DriverManager.getConnection(
                URL, username, password);
        System.out.println("Connected to database");
        return conn;
    }

    
}
