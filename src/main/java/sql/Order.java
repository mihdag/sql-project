// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;


public class Order {
    private int order_id;
    private int product_id;
    private int quantity;
    private double price;
    private String storeOrdered;


    public Order() {
    }


    public Order(int order_id, int product_id, int quantity, double price, String storeOrdered) {
        this.order_id = order_id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.price = price;
        this.storeOrdered = storeOrdered;
    }

    @Override
    public String toString() {
        return 
            " order_id = '" + getOrder_id() + "'" +
            ", product_id = '" + getProduct_id() + "'" +
            ", quantity = '" + getQuantity() + "'" +
            ", price = '" + getPrice() + "'" +
            ", storeOrdered = '" + getStoreOrdered() + "'";
    }


    public int getOrder_id() {
        return this.order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStoreOrdered() {
        return this.storeOrdered;
    }

    public void setStoreOrdered(String storeOrdered) {
        this.storeOrdered = storeOrdered;
    }

    // get the number of time the product got ordered
    public static int getNumberOfOrders (Connection conn) throws SQLException {
        System.out.println("What product you are looking for?");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        sc.close();
        String query = "SELECT * FROM orders JOIN products ON orders.product_id = products.product_id WHERE name = '" + name + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int count = 0;
        while (rs.next()) {
            count++;
        }
        return count;
    }
    
    //Validate if the product is at 0. if yes make an order
    public static void validateOrder(Connection conn) throws SQLException {
         System.out.println("What productID are you looking to validate?");
        Scanner sc = new Scanner(System.in);
        int ID = sc.nextInt();
        sc.close();
        String query = "SELECT sum(quantity) AS total_quantity FROM inventory WHERE product_id = '" + ID + "' GROUP BY product_id";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
       double quantity = 0;
        while (rs.next()) {
            quantity = rs.getDouble("total_quantity");
        }
        if (quantity == 0.0) {
            System.out.println("An order has been made for this product.");
        }
        else {
            System.out.println("There is still some quanity in the inventory");
        }
        
    }

    // Display order by selected stores
    public static ArrayList<Order> displayOrderByStore (Connection conn) throws SQLException {
        System.out.println("What store do you want to check the order?");
        Scanner sc = new Scanner(System.in);
        String store = sc.nextLine();
        ArrayList<Order> orders = new ArrayList<Order>();
        sc.close();
        String query = "SELECT * FROM orders WHERE store_ordered = '" + store + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            int order_id = rs.getInt("order_id");
            int product_id = rs.getInt("product_id");
            int quantity = rs.getInt("quantity");
            double price = rs.getDouble("price");
            String storeOrdered = rs.getString("store_ordered");
            Order order = new Order(order_id,product_id,quantity,price,storeOrdered);
            orders.add(order);
        }
        return orders;   
    }

    // Display order by their customers
    public static ArrayList<Order> displayOrderByCustomer (Connection conn) throws SQLException {
        System.out.println("What CustomerID would you like to display their order ?");
        Scanner sc = new Scanner(System.in);
        int ID = sc.nextInt();
        ArrayList<Order> orders = new ArrayList<Order>();
        sc.close();
        String query = "SELECT * FROM orders WHERE customer_id = '" + ID + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            int order_id = rs.getInt("order_id");
            int product_id = rs.getInt("product_id");
            int quantity = rs.getInt("quantity");
            double price = rs.getDouble("price");
            String storeOrdered = rs.getString("store_ordered");
            Order order = new Order(order_id,product_id,quantity,price,storeOrdered);
            orders.add(order);
        }
        return orders;   
    }

    // method to add orders
    public static void addOrder (Connection conn) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("What customerID is the order assosiated with?");
        int customer = sc.nextInt();
        System.out.println("What ProductID is in the order?");
        int product = sc.nextInt();
        System.out.println("What ReviewID is the order assosiated with?");
        int review = sc.nextInt();
        System.out.println("What is the quantity of the order?");
        int quantity = sc.nextInt();
        System.out.println("What's the price of the order?");
        double price = sc.nextDouble();
        System.out.println("What store is the order from?");
        sc.nextLine();
        String store = sc.nextLine();
        LocalDate date = LocalDate.now();

        sc.close();
        String query = "SELECT COUNT(*) FROM orders";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("COUNT(*)");
        }
        count = count + 1;

        String stm = "{call order_data.add_order(" +count+", "+customer+", " +product+ ", " +review+ ", " +quantity+ ", " +price+", '" + store+ "' , TO_DATE('" +date+"', 'YYYY-MM-DD'))}";
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("Order added successfully.");

           }
    }

    // method to delete order data
    public static void deleteOrder (Connection conn) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("What OrderID you would like to delete?");
        int ID = sc.nextInt();

        sc.close();

        String stm = String.format("{call order_data.delete_order(%d)}",ID);
        try (CallableStatement callableStatement = conn.prepareCall(stm)) {
           // Execute the stored procedure
           callableStatement.executeUpdate();

           System.out.println("Order deleted successfully.");

           }
    }
}
