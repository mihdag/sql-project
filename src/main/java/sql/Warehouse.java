// David Hang, 2234338, Dang Minh Nguyen, 2133544
package sql;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Warehouse {
    String warehouse_id;
    String address;
    public Warehouse(String warehouse_id, String address) {
        this.warehouse_id = warehouse_id;
        this.address = address;
    }
    public String getWarehouse_id() {
        return warehouse_id;
    }
    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    // delete the warehouse with the inventory included
    public static void deleteWarehouse(Connection conn) throws SQLException {
        System.out.println("What warehouse you want to delete?");
        Scanner sc = new Scanner(System.in);
        String warehouse_id = sc.nextLine();
   
               String stm = String.format("{call warehouse_data.delete_warehouse('%s')}", warehouse_id);
         try (CallableStatement callableStatement = conn.prepareCall(stm)) {
            // Execute the stored procedure
            callableStatement.executeUpdate();

            System.out.println("warehouse deleted successfully.");

            }

        sc.close();
    }

    //Updating warehouse
    //update from inventory
    public static void updateWarehouse (Connection conn) throws SQLException {
        System.out.println("What warehouse you want to update?");
        Scanner sc = new Scanner(System.in);
        String warehouse_id = sc.nextLine();
        System.out.println("What would you like to change the address for?");
        String address = sc.nextLine();

    //update_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT);
    String stm = String.format("{call warehouse_data.update_warehouse('%s', '%s')}", warehouse_id, address);
    try (CallableStatement callableStatement = conn.prepareCall(stm)) {
        // Execute the stored procedure
        callableStatement.executeUpdate();

        System.out.println("warehouse updated successfully.");

        }
    }
}
