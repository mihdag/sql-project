-- David Hang, 2234338, Dang Minh Nguyen, 2133544
-- Create all tables for the setup
CREATE TABLE customers (
    customer_id int PRIMARY KEY,
    firstname varchar2(255),
    lastname varchar2(255)
);

CREATE TABLE address (
    customer_id int,
    address varchar2(255),
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

CREATE TABLE email (
    customer_id int,
    email varchar2(255),
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

CREATE TABLE reviews (
    review_id int PRIMARY KEY,
    review_flag int,
    description varchar2(255),
    rating int
);

CREATE TABLE products (
    product_id int PRIMARY KEY,
    name varchar2(255),
    category varchar2(255)
);

CREATE TABLE orders(
    order_id int PRIMARY KEY,
    customer_id int,
    product_id int,
    review_id int,
    quantity int,
    price number,
    store_ordered varchar2(255),
    order_date date,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id),
    FOREIGN KEY (review_id) REFERENCES reviews(review_id)
);

CREATE TABLE warehouse (
    warehouse_id varchar2(255) PRIMARY KEY,
    address varchar2(255)
);

CREATE TABLE inventory (
    warehouse_id varchar2(255),
    product_id int,
    quantity int,
    FOREIGN KEY (warehouse_id) REFERENCES warehouse(warehouse_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);

-- Used for audit log 
CREATE TABLE audit_log (
    audit_id INT PRIMARY KEY,
    table_name VARCHAR2(255),
    operation VARCHAR2(10),
    user_name VARCHAR2(255),
    operation_time TIMESTAMP,
    details VARCHAR2(4000) 
);

-- Sequence for audit Id
CREATE SEQUENCE seq_audit_id
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

-- Inserting data into customers table
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(1, 'mahsa', 'sadeghi');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(2, 'alex', 'brown');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(3, 'martin', 'alexandre');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(4, 'daneil', 'hanne');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(5, 'John', 'boura');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(6, 'Ari', 'brown');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(7, 'Amanda', 'Harry');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(8, 'Jack', 'Jonhson');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(9, 'John', 'belle');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(10, 'martin', 'Li');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(11, 'olivia', 'smith');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(12, 'Noah', 'Garcia');
-- Local info
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(13, 'Amanda', 'Harry');
INSERT INTO customers (customer_id, firstname, lastname) VALUES
(14, 'Jack', 'Jonhson');

-- Inserting data into email table
INSERT INTO email (customer_id, email) VALUES
(1, 'msadeghi@dawsoncollege.qc.ca');
INSERT INTO email (customer_id, email) VALUES
(2, 'alex@gmail.com');
INSERT INTO email (customer_id, email) VALUES
(3, 'marting@yahoo.com');
INSERT INTO email (customer_id, email) VALUES
(4, 'daneil@yahoo.com');
INSERT INTO email (customer_id, email) VALUES
(5, 'bdoura@gmail.com');
INSERT INTO email (customer_id, email) VALUES
(6, 'b.a@gmail.com');
INSERT INTO email (customer_id, email) VALUES
(7, 'am.harry@yahioo.com');
INSERT INTO email (customer_id, email) VALUES
(8, 'johnson.a@gmail.com');
INSERT INTO email (customer_id, email) VALUES
(1, 'ms@gmail.com');
INSERT INTO email (customer_id, email) VALUES
(9, 'abcd@yahoo.com');
INSERT INTO email (customer_id, email) VALUES
(10, 'm.li@gmail.com');
INSERT INTO email (customer_id, email) VALUES
(11, 'smith@hotmail.com');
INSERT INTO email (customer_id, email) VALUES
(12, 'g.noah@yahoo.com');

-- Inserting data into address table
INSERT INTO address (customer_id, address) VALUES
(1, 'dawson college, montreal, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(2, '090 boul saint laurent, montreal, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(3, 'brossard, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(4, '100 atwater street, toronto, canada');
INSERT INTO address (customer_id, address) VALUES
(2, 'boul saint laurent, montreal, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(5, '100 Young street, toronto, canada');
INSERT INTO address (customer_id, address) VALUES
(6, NULL);
INSERT INTO address (customer_id, address) VALUES
(7, '100 boul saint laurent, montreal, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(8, 'Calgary, Alberta, Canada');
INSERT INTO address (customer_id, address) VALUES
(1, '104 gill street, Toronto, Canada');
INSERT INTO address (customer_id, address) VALUES
(9, '105 Young street, toronto, canada');
INSERT INTO address (customer_id, address) VALUES
(2, 'boul saint laurent, montreal, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(10, '87 boul saint laurent, montreal, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(11, '76 boul decalthon, laval, quebec, canada');
INSERT INTO address (customer_id, address) VALUES
(12, '22222 happy street, Laval, quebec, canada');

-- Inserting data into products table
INSERT INTO products (product_id, name, category) VALUES
(1, 'laptop ASUS 104S', 'electronics');
INSERT INTO products (product_id, name, category) VALUES
(2, 'apple', 'Grocery');
INSERT INTO products (product_id, name, category) VALUES
(3, 'SIMS CD', 'Video Games');
INSERT INTO products (product_id, name, category) VALUES
(4, 'orange', 'grocery');
INSERT INTO products (product_id, name, category) VALUES
(5, 'Barbie Movie', 'DVD');
INSERT INTO products (product_id, name, category) VALUES
(6, 'L Oreal Normal Hair', 'Health');
INSERT INTO products (product_id, name, category) VALUES
(7, 'BMW iX Lego', 'Toys');
INSERT INTO products (product_id, name, category) VALUES
(8, 'BMW i6', 'Cars');
INSERT INTO products (product_id, name, category) VALUES
(9, 'Truck 500c', 'Vehicle');
INSERT INTO products (product_id, name, category) VALUES
(10, 'paper towel', 'Beauty');
INSERT INTO products (product_id, name, category) VALUES
(11, 'plum', 'grocery');
INSERT INTO products (product_id, name, category) VALUES
(12, 'Lamborghini Lego', 'Toys');
INSERT INTO products (product_id, name, category) VALUES
(13, 'chicken', 'grocery');
INSERT INTO products (product_id, name, category) VALUES
(14, 'pasta', 'Grocery');
INSERT INTO products (product_id, name, category) VALUES
(15, 'PS5', 'electronics');
INSERT INTO products (product_id, name, category) VALUES
(16, 'tomato', NULL);
INSERT INTO products (product_id, name, category) VALUES
(17, 'Train X745', NULL);
-- Local info
INSERT INTO products (product_id, name, category) VALUES
(18, 'beef', 'grocery');
INSERT INTO products (product_id, name, category) VALUES
(19, 'chicken', 'grocery');

-- Inserting data into reviews table
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(1, 0, 'it was affordable.', 4);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(2, 0, 'quality was not good', 3);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(3, 1, NULL, 2);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(4, 0, 'highly recommend', 5);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(5, 0, NULL, 1);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(6, 0, 'did not worth the price', 1);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(7, 0, 'missing some parts', 1);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(8, 1, 'trash', 5);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(9, NULL, NULL, 2);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(10, NULL, NULL, 5);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(11, NULL, NULL, 4);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(12, NULL, NULL, 3);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(13, 0, 'missing some parts', 1);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(14, NULL, NULL, 4);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(15, 0, 'great product', 1);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(16, 1, 'bad quality', 5);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(17, 0, NULL, 1);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(18, 0, NULL, 4);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(19, NULL, NULL, 4);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(20, NULL, NULL, 5);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(21, 2, 'worse car i have droven!', 5);
INSERT INTO reviews (review_id, review_flag, description, rating) VALUES
(22, NULL, NULL, 4);

-- Inserting data into orders table
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(1, 1, 1, 1, 1, 970, 'marche adonis', TO_DATE('2023-04-21', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(2, 2, 2, 2, 2, 10, 'marche atwater', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(3, 3, 3, 3, 3, 50, 'dawson store', TO_DATE('2023-10-01', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(4, 4, 4, 4, 1, 2, 'store magic', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(5, 2, 5, 5, 1, 30, 'movie store', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(6, 3, 6, 6, 1, 10, 'super rue champlain', TO_DATE('2023-10-10', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(7, 1, 7, 7, 1, 40, 'toy r us', TO_DATE('2023-10-11', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(8, 5, 8, 8, 1, 50000, 'Dealer one', TO_DATE('2023-10-10', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(9, 6, 9, 9, 1, 856600, 'dealer montreal', NULL);
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(10, 7, 10, 10, 3, 50, 'movie start', NULL);
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(11, 8, 11, 11, 6, 10, 'marche atwater', TO_DATE('2020-05-06', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(12, 3, 6, 12, 3, 30, 'super rue champlain', TO_DATE('2019-09-12', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(13, 1, 12, 13, 1, 40, 'toy r us', TO_DATE('2010-10-11', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(14, 1, 11, 14, 7, 10, 'marche atwater', TO_DATE('2022-05-06', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(15, 1, 12, 15, 2, 80, 'toy r us', TO_DATE('2023-10-07', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(16, 9, 8, 16, 1, 50000, 'Dealer one', TO_DATE('2023-08-10', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(17, 2, 3, 17, 1, 16, 'movie store', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(18, 2, 5, 18, 1, 45, 'toy r us', TO_DATE('2023-10-02', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(19, 10, 13, 19, 1, 9.5, 'marche adonis', TO_DATE('2019-04-03', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(20, 11, 14, 20, 3, 13.5, 'marche atwater', TO_DATE('2021-12-29', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(21, 12, 15, NULL, 1, 200, 'star store', TO_DATE('2020-01-20', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(22, 1, 7, 21, 1, 38, 'toy r us', TO_DATE('2022-10-11', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(23, 11, 14, 22, 3, 15, 'store magic', TO_DATE('2021-12-29', 'YYYY-MM-DD'));
-- Local info
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(24, 7, 10, 20, 31, 76, 'toy r us', TO_DATE('2023-11-19', 'YYYY-MM-DD'));
INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date) VALUES
(25, 1, 3, 5, 6, 700, 'star store', TO_DATE('2022-10-8', 'YYYY-MM-DD'));

-- Inserting data into warehouse table
INSERT INTO warehouse (warehouse_id, address) VALUES
('A', '100 rue William, saint laurent, Quebec, Canada');
INSERT INTO warehouse (warehouse_id, address) VALUES
('B', '304 Rue François-Perrault, Villera Saint-Michel, Montréal, QC');
INSERT INTO warehouse (warehouse_id, address) VALUES
('C', '86700 Weston Rd, Toronto, Canada');
INSERT INTO warehouse (warehouse_id, address) VALUES
('D', '170  Sideroad, Quebec City, Canada');
INSERT INTO warehouse (warehouse_id, address) VALUES
('E', '1231 Trudea road, Ottawa, Canada');
INSERT INTO warehouse (warehouse_id, address) VALUES
('F', '16  Whitlock Rd, Alberta, Canada');

-- Inserting data into inventory table
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('A', 1, 1000);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('B', 2, 24980);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('C', 3, 103);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('D', 4, 35405);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('E', 5, 40);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('F', 6, 450);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('A', 7, 10);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('A', 8, 6);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('E', 9, 1000);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('F', 10, 3532);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('C', 11, 43242);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('B', 10, 39484);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('D', 11, 6579);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('E', 12, 98765);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('F', 13, 43523);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('A', 14, 2132);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('D', 15, 123);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('A', 16, 352222);
INSERT INTO inventory (warehouse_id, product_id, quantity) VALUES
('E', 17, 4543);

-- Packages for interacting with customer
CREATE OR REPLACE PACKAGE customer_data AS
    PROCEDURE add_customer(p_customer_id INT, p_firstname VARCHAR2, p_lastname VARCHAR2);
    PROCEDURE update_customer(p_customer_id INT, p_firstname VARCHAR2, p_lastname VARCHAR2);
    PROCEDURE delete_customer(p_customer_id INT);
    PROCEDURE flagged_customers;
END customer_data;
/

CREATE OR REPLACE PACKAGE BODY customer_data AS
-- PROCEDURE FOR CUSTOMER DATA
    PROCEDURE add_customer(p_customer_id INT, p_firstname VARCHAR2, p_lastname VARCHAR2) AS
        BEGIN
            INSERT INTO customers (customer_id, firstname, lastname)
            VALUES (p_customer_id, p_firstname, p_lastname);
        END add_customer;

    PROCEDURE update_customer(p_customer_id INT, p_firstname VARCHAR2, p_lastname VARCHAR2) AS
        BEGIN
            UPDATE customers
            SET firstname = p_firstname, lastname = p_lastname
            WHERE customer_id = p_customer_id;
        END update_customer;

    PROCEDURE delete_customer(p_customer_id INT) AS
        BEGIN
            DELETE FROM customers WHERE customer_id = p_customer_id;
        END delete_customer;
        
    PROCEDURE flagged_customers AS
        BEGIN
            FOR customer_rec IN (SELECT c.customer_id,
                                        c.firstname,
                                        c.lastname, 
                                        r.description as description
                                FROM customers c
                                    INNER JOIN orders o 
                                    ON c.customer_id = o.customer_id
                                    INNER JOIN reviews r
                                    ON o.review_id = r.review_id
                                WHERE r.review_flag >= 1)
            LOOP
                DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_rec.customer_id ||
                             ', Name: ' || customer_rec.firstname || ' ' || customer_rec.lastname || ',review: ' || customer_rec.description);
            END LOOP;
        EXCEPTION
            WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
        END flagged_customers;
        
END customer_data;
/


-- Packages for interacting with inventory
CREATE OR REPLACE PACKAGE inventory_data AS
    PROCEDURE add_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT);
    PROCEDURE update_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT);
    PROCEDURE delete_inventory(p_warehouse_id VARCHAR2, p_product_id INT);
    PROCEDURE display_inventory_by_product_id(p_product_id INT);
END inventory_data;
/

CREATE OR REPLACE PACKAGE BODY inventory_data AS
-- PROCEDURE FOR INVENTORY DATA
    PROCEDURE add_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT) AS
        BEGIN
            INSERT INTO inventory (warehouse_id, product_id, quantity)
            VALUES (p_warehouse_id, p_product_id, p_quantity);
        END add_inventory;

    PROCEDURE update_inventory(p_warehouse_id VARCHAR2, p_product_id INT, p_quantity INT) AS
        BEGIN
            UPDATE inventory
            SET quantity = p_quantity
            WHERE warehouse_id = p_warehouse_id AND product_id = p_product_id;
        END update_inventory;

    PROCEDURE delete_inventory(p_warehouse_id VARCHAR2, p_product_id INT) AS
        BEGIN
            DELETE FROM inventory WHERE warehouse_id = p_warehouse_id AND product_id = p_product_id;
        END delete_inventory;
        
    PROCEDURE display_inventory_by_product_id(p_product_id INT) AS
        -- Declare a variable to store the product details
        inventory_record inventory%ROWTYPE;

        -- Declare a cursor with a parameter
        CURSOR inventory_cursor (p_product_id INT) IS
            SELECT warehouse_id, product_id, quantity
            FROM inventory
            WHERE product_id = p_product_id;
        BEGIN
            -- Open the cursor with the parameter value
            OPEN inventory_cursor(p_product_id);
    
            -- Fetch records using the cursor
            LOOP
                FETCH inventory_cursor INTO inventory_record;
                EXIT WHEN inventory_cursor%NOTFOUND;

            -- Display inventory details in a user-friendly way
            DBMS_OUTPUT.PUT_LINE('Warehouse ID: ' || inventory_record.warehouse_id|| ' Product ID: ' || inventory_record.product_id || ' Categoty: ' || inventory_record.quantity);
        END LOOP;
        
                -- Close the cursor
        CLOSE inventory_cursor;
            EXCEPTION
            -- Catch and handle exceptions
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
    END display_inventory_by_product_id;
    
    
END inventory_data;
/


-- Packages for interacting orders
CREATE OR REPLACE PACKAGE order_data AS
    PROCEDURE add_order(p_order_id INT, p_customer_id INT, p_product_id INT, p_review_id INT, p_quantity INT, p_price NUMBER, p_store_ordered VARCHAR2, p_order_date DATE);
    PROCEDURE update_order(p_order_id INT, p_customer_id INT, p_product_id INT, p_review_id INT, p_quantity INT, p_price NUMBER, p_store_ordered VARCHAR2, p_order_date DATE);
    PROCEDURE delete_order(p_order_id INT);
END order_data;
/

CREATE OR REPLACE PACKAGE BODY order_data AS

-- PROCEDURE FOR ORDERS DATA
    PROCEDURE add_order(p_order_id INT, p_customer_id INT, p_product_id INT, p_review_id INT, p_quantity INT, p_price NUMBER, p_store_ordered VARCHAR2, p_order_date DATE) AS
        BEGIN
            INSERT INTO orders (order_id, customer_id, product_id, review_id, quantity, price, store_ordered, order_date)
            VALUES (p_order_id, p_customer_id, p_product_id, p_review_id, p_quantity, p_price, p_store_ordered, p_order_date);
        END add_order;
    
    PROCEDURE update_order(p_order_id INT, p_customer_id INT, p_product_id INT, p_review_id INT, p_quantity INT, p_price NUMBER, p_store_ordered VARCHAR2, p_order_date DATE) AS
        BEGIN
            UPDATE orders
            SET customer_id = p_customer_id, product_id = p_product_id, review_id = p_review_id, quantity = p_quantity, price = p_price, store_ordered = p_store_ordered, order_date = p_order_date
            WHERE order_id = p_order_id;
        END update_order;
    
    PROCEDURE delete_order(p_order_id INT) AS
        BEGIN
            DELETE FROM orders WHERE order_id = p_order_id;
        END delete_order;
        
END order_data;
/

-- Packages for interacting product
CREATE OR REPLACE PACKAGE product_data AS
    PROCEDURE add_product(p_product_id INT, p_name VARCHAR2, p_category VARCHAR2);
    PROCEDURE update_product(p_product_id INT, p_name VARCHAR2, p_category VARCHAR2);
    PROCEDURE delete_product(p_product_id INT);
    PROCEDURE display_products_by_category(p_category VARCHAR2);
END product_data;
/

--Procedure for products
CREATE OR REPLACE PACKAGE BODY product_data AS
    PROCEDURE add_product(p_product_id INT, p_name VARCHAR2, p_category VARCHAR2) AS
        BEGIN
            INSERT INTO products (product_id, name, category)
            VALUES (p_product_id, p_name, p_category);
        END add_product;

    PROCEDURE update_product(p_product_id INT, p_name VARCHAR2, p_category VARCHAR2) AS
        BEGIN
            UPDATE products
            SET name = p_name, category = p_category
            WHERE product_id = p_product_id;
        END update_product;

    PROCEDURE delete_product(p_product_id INT) AS
        BEGIN
            DELETE FROM products WHERE product_id = p_product_id;
        END delete_product;
        
    PROCEDURE display_products_by_category(p_category VARCHAR2) AS
        -- Declare a variable to store the product details
        product_record products%ROWTYPE;

        -- Declare a cursor with a parameter
        CURSOR product_cursor (p_category VARCHAR2) IS
            SELECT product_id, name, category
            FROM products
            WHERE category = p_category;

        BEGIN
            -- Open the cursor with the parameter value
            OPEN product_cursor(p_category);
    
            -- Fetch records using the cursor
            LOOP
                FETCH product_cursor INTO product_record;
                EXIT WHEN product_cursor%NOTFOUND;

            -- Display product details in a user-friendly way
            DBMS_OUTPUT.PUT_LINE('Product ID: ' || product_record.product_id || ' Name: ' || product_record.name || ' Category: ' || product_record.category);
        END LOOP;

        -- Close the cursor
        CLOSE product_cursor;
            EXCEPTION
            -- Catch and handle exceptions
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
    END display_products_by_category;

END product_data;
/

-- Packages for interacting with review
CREATE OR REPLACE PACKAGE review_data AS
    PROCEDURE add_review(p_review_id INT, p_review_flag INT, p_description VARCHAR2, p_rating INT);
    PROCEDURE update_review(p_review_id INT, p_review_flag INT, p_description VARCHAR2, p_rating INT);
    PROCEDURE delete_review(p_review_id INT);
END review_data;
/

CREATE OR REPLACE PACKAGE BODY review_data AS

-- Procedure for review
    PROCEDURE add_review(p_review_id INT, p_review_flag INT, p_description VARCHAR2, p_rating INT) AS
        BEGIN
            INSERT INTO reviews (review_id, review_flag, description, rating)
            VALUES (p_review_id, p_review_flag, p_description, p_rating);
        END add_review;

    PROCEDURE update_review(p_review_id INT, p_review_flag INT, p_description VARCHAR2, p_rating INT) AS
        BEGIN
            UPDATE reviews
            SET review_flag = p_review_flag, description = p_description, rating = p_rating
            WHERE review_id = p_review_id;
        END update_review;

    PROCEDURE delete_review(p_review_id INT) AS
        BEGIN
            DELETE FROM reviews WHERE review_id = p_review_id;
        END delete_review;

END review_data;
/

-- Packages for interacting with warehouse
CREATE OR REPLACE PACKAGE warehouse_data AS
    PROCEDURE add_warehouse(p_warehouse_id VARCHAR2, p_address VARCHAR2);
    PROCEDURE update_warehouse(p_warehouse_id VARCHAR2, p_address VARCHAR2);
    PROCEDURE delete_warehouse(p_warehouse_id VARCHAR2);
END warehouse_data;
/

CREATE OR REPLACE PACKAGE BODY warehouse_data AS

-- Procedure for warehouse
    PROCEDURE add_warehouse(p_warehouse_id VARCHAR2, p_address VARCHAR2) AS
        BEGIN
            INSERT INTO warehouse (warehouse_id, address)
            VALUES (p_warehouse_id, p_address);
        END add_warehouse;

    PROCEDURE update_warehouse(p_warehouse_id VARCHAR2, p_address VARCHAR2) AS
        BEGIN
            UPDATE warehouse
            SET address = p_address
            WHERE warehouse_id = p_warehouse_id;
        END update_warehouse;

    PROCEDURE delete_warehouse(p_warehouse_id VARCHAR2) AS
        BEGIN
            DELETE FROM inventory WHERE warehouse_id = p_warehouse_id;
            DELETE FROM warehouse WHERE warehouse_id = p_warehouse_id;
        END delete_warehouse;

END warehouse_data;
/


-- Packages used to display data
CREATE OR REPLACE PACKAGE display_data AS
    PROCEDURE display_inventory(p_product_id INT);
END display_data;
/
CREATE OR REPLACE PACKAGE BODY display_data AS

-- Procedure to display data from the database
    PROCEDURE display_inventory(p_product_id INT) IS
        total_inventory INT;
    BEGIN
        SELECT SUM(quantity)
        INTO total_inventory
        FROM inventory
        WHERE product_id = p_product_id;
    
        DBMS_OUTPUT.PUT_LINE('Total Inventory for Product ID ' || p_product_id || ' is: ' || total_inventory);
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
    END;
END display_data;
/

-- Packages used to calculate data
CREATE OR REPLACE PACKAGE function_data AS
    FUNCTION avg_product_rating(p_product_id INT) RETURN NUMBER;
    FUNCTION total_inventory(p_product_id INT) RETURN INT;
    FUNCTION product_order_count(p_product_id INT) RETURN INT;
END function_data;
/
CREATE OR REPLACE PACKAGE BODY function_data AS

-- functions for data
    FUNCTION avg_product_rating(p_product_id INT) RETURN NUMBER AS
        v_avg_rating NUMBER;
    BEGIN
        SELECT AVG(r.rating)
        INTO v_avg_rating
        FROM orders o
        INNER JOIN reviews r 
        ON o.review_id = r.review_id
        WHERE product_id = p_product_id;
    
        RETURN v_avg_rating;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
    END avg_product_rating;

    FUNCTION total_inventory(p_product_id INT) RETURN INT AS
        v_total_inventory INT;
    BEGIN
        SELECT SUM(quantity)
        INTO v_total_inventory
        FROM inventory
        WHERE product_id = p_product_id;
    
        RETURN v_total_inventory;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN 0;
    END total_inventory;

    FUNCTION product_order_count(p_product_id INT) RETURN INT AS
        v_order_count INT;
    BEGIN
        SELECT COUNT(*)
        INTO v_order_count
        FROM orders
        WHERE product_id = p_product_id;
    
        RETURN v_order_count;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN 0;
    END product_order_count;

END function_data;
/


-- Trigger for order
CREATE OR REPLACE TRIGGER orders_audit_insert
AFTER INSERT ON orders
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'orders', 'INSERT', USER, SYSTIMESTAMP, 'Order inserted with ID: ' || :NEW.order_id);
END;
/

-- Trigger for UPDATE order
CREATE OR REPLACE TRIGGER orders_audit_update
AFTER UPDATE ON orders
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'orders', 'UPDATE', USER, SYSTIMESTAMP, 'Order updated with ID: ' || :NEW.order_id);
END;
/

-- Trigger for DELETE order
CREATE OR REPLACE TRIGGER orders_audit_delete
AFTER DELETE ON orders
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'orders', 'DELETE', USER, SYSTIMESTAMP, 'Order deleted with ID: ' || :OLD.order_id);
END;
/

-- customer trigger
CREATE OR REPLACE TRIGGER audit_customers_insert
AFTER INSERT ON customers
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'customers', 'INSERT', USER, SYSTIMESTAMP, 'Customer inserted with ID: ' || :NEW.customer_id);
END;
/

CREATE OR REPLACE TRIGGER audit_customers_update
AFTER UPDATE ON customers
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'customers', 'UPDATE', USER, SYSTIMESTAMP, 'Customer updated with ID: ' || :NEW.customer_id);
END;
/

CREATE OR REPLACE TRIGGER audit_customers_delete
AFTER DELETE ON customers
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'customers', 'DELETE', USER, SYSTIMESTAMP, 'Customer deleted with ID: ' || :OLD.customer_id);
END;
/

-- Product trigger
CREATE OR REPLACE TRIGGER audit_products_insert
AFTER INSERT ON products
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'products', 'INSERT', USER, SYSTIMESTAMP, 'Product inserted with ID: ' || :NEW.product_id || ', Name: ' || :NEW.name || ', Category: ' || :NEW.category);
END;
/

CREATE OR REPLACE TRIGGER audit_products_update
AFTER UPDATE ON products
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'products', 'UPDATE', USER, SYSTIMESTAMP, 'Product updated with ID: ' || :NEW.product_id || ', Name: ' || :NEW.name || ', Category: ' || :NEW.category);
END;
/

CREATE OR REPLACE TRIGGER audit_products_delete
AFTER DELETE ON products
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'products', 'DELETE', USER, SYSTIMESTAMP, 'Product deleted with ID: ' || :OLD.product_id || ', Name: ' || :OLD.name || ', Category: ' || :OLD.category);
END;
/

-- Inventory trigger
CREATE OR REPLACE TRIGGER audit_inventory_insert
AFTER INSERT ON inventory
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'inventory', 'INSERT', USER, SYSTIMESTAMP, 'Inventory record inserted for Warehouse ID: ' || :NEW.warehouse_id || ', Product ID: ' || :NEW.product_id || ', Quantity: ' || :NEW.quantity);
END;
/

CREATE OR REPLACE TRIGGER audit_inventory_update
AFTER UPDATE ON inventory
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'inventory', 'UPDATE', USER, SYSTIMESTAMP, 'Inventory record updated for Warehouse ID: ' || :NEW.warehouse_id || ', Product ID: ' || :NEW.product_id || ', Quantity: ' || :NEW.quantity);
END;
/

CREATE OR REPLACE TRIGGER audit_inventory_delete
AFTER DELETE ON inventory
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'inventory', 'DELETE', USER, SYSTIMESTAMP, 'Inventory record deleted for Warehouse ID: ' || :OLD.warehouse_id || ', Product ID: ' || :OLD.product_id);
END;
/

-- Review trigger
CREATE OR REPLACE TRIGGER audit_reviews_insert
AFTER INSERT ON reviews
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'reviews', 'INSERT', USER, SYSTIMESTAMP, 'Review inserted with ID: ' || :NEW.review_id || ', Flag: ' || :NEW.review_flag || ', Description: ' || :NEW.description);
END;
/

CREATE OR REPLACE TRIGGER audit_reviews_update
AFTER UPDATE ON reviews
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'reviews', 'UPDATE', USER, SYSTIMESTAMP, 'Review updated with ID: ' || :NEW.review_id || ', Flag: ' || :NEW.review_flag || ', Description: ' || :NEW.description);
END;
/

CREATE OR REPLACE TRIGGER audit_reviews_delete
AFTER DELETE ON reviews
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'reviews', 'DELETE', USER, SYSTIMESTAMP, 'Review deleted with ID: ' || :OLD.review_id || ', Flag: ' || :OLD.review_flag || ', Description: ' || :OLD.description);
END;
/

-- Warehouse trigger
CREATE OR REPLACE TRIGGER audit_warehouse_insert
AFTER INSERT ON warehouse
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'warehouse', 'INSERT', USER, SYSTIMESTAMP, 'Warehouse inserted with ID: ' || :NEW.warehouse_id || ', Address: ' || :NEW.address);
END;
/

CREATE OR REPLACE TRIGGER audit_warehouse_update
AFTER UPDATE ON warehouse
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'warehouse', 'UPDATE', USER, SYSTIMESTAMP, 'Warehouse updated with ID: ' || :NEW.warehouse_id || ', Address: ' || :NEW.address);
END;
/

CREATE OR REPLACE TRIGGER audit_warehouse_delete
AFTER DELETE ON warehouse
FOR EACH ROW
BEGIN
    INSERT INTO audit_log (audit_id, table_name, operation, user_name, operation_time, details)
    VALUES (seq_audit_id.NEXTVAL, 'warehouse', 'DELETE', USER, SYSTIMESTAMP, 'Warehouse deleted with ID: ' || :OLD.warehouse_id);
END;
/
















